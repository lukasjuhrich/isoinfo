use std::{io::{BufReader, Seek, SeekFrom, Read}, path::{Path, PathBuf}};

use clap::{Parser, Subcommand};
use packed_struct::prelude::*;

#[derive(Parser)]
#[clap(about, version, author)]
struct Args {
    #[clap(subcommand)]
    command: Command,
}


#[derive(Subcommand)]
enum Command {
    // TODO replace u64 by something which has `FromStr`
    // …and accepts hex numbers like 0x8000
    VolDesc { offset: u64, filename: PathBuf },
}

fn main() {
    let args = Args::parse();

    match &args.command {
        Command::VolDesc { offset, filename } => {
            match load_path(filename) {
                Err(_) => { std::process::exit(1); }
                Ok((mut reader, _)) => {
                    reader
                        .seek(SeekFrom::Start(*offset))
                        .expect("seek didn't work");

                    let buf = {
                        let mut buf = [0u8; 2048];
                        reader
                            .read_exact(&mut buf)
                            .expect("read_exact didn't work");
                        buf
                    };

                    use VolumeDescriptor::*;
                    let desc = VolumeDescriptor::from_buf(&buf);
                    match desc {
                        Primary(p) => {
                            println!("Primary volume descriptor.");
                            println!("System info: {:#?}", p.system_identifier());
                            println!("Volume info: {:#?}", p.volume_identifier());
                            println!("Data preparer: {:#?}", p.data_preparer_identifier());
                            println!("application_identifier: {:#?}", p.application_identifier());
                            println!("© file id: {:#?}", _bad_to_str(p.copyright_file_id.as_slice()));
                            println!("abstract file id: {:#?}", _bad_to_str(p.abstract_file_id.as_slice()));
                            println!("bib file id: {:#?}", _bad_to_str(p.bib_file_id.as_slice()));
                            println!("volume created at: {:#?}", _bad_to_str(p.vol_created_dt.as_slice()));
                            println!("volume modified at: {:#?}", _bad_to_str(p.vol_mod_dt.as_slice()));
                            println!("volume expired at: {:#?}", _bad_to_str(p.vol_expir_dt.as_slice()));
                            println!("volume effective at: {:#?}", _bad_to_str(p.vol_effective_dt.as_slice()));
                        },
                        BootRecord => println!("BootRecord. Further info to be implemented."),
                        _ => (),
                    }
                }
            }
        },
    }
}


pub fn load_path<T: AsRef<Path>>(path: T) -> Result<(BufReader<std::fs::File>, u64), std::io::Error> {
    let file = std::fs::File::open(path)?;
    let file_len = file.metadata()?.len();
    Ok((BufReader::new(file), file_len))
}

enum VolumeDescriptor {
    BootRecord,
    Primary(Box<PrimaryVolumeDescriptor>),
    // Supplementary volume descriptor
    Supplementary,
    // partition
    Partition,
    // Volume Descriptor Set Terminator
    SetTerminator,

    Reserved,
}

impl VolumeDescriptor {
    pub fn from_buf(buf: &[u8; 2048]) -> Self {
        // 0x0..+1: type
        // 0x1..+5: magic ('CD001')
        // 0x6..+1: version (0x1)
        // 0x7..+2041: data
        match buf[0] {
            0 => Self::BootRecord,
            1 => {
                let mut a= [0u8; 881];
                a.copy_from_slice(&buf[0..881]);
                Self::Primary(
                    Box::new(PrimaryVolumeDescriptor::unpack(&a)
                    .expect("parsing error"))
                )
            },
            2 => Self::Supplementary,
            3 => Self::Partition,
            255 => Self::SetTerminator,
            _ => Self::Reserved,
        }
    }
}

#[derive(PackedStruct)]
#[packed_struct(bit_numbering="msb0", endian="lsb")]
pub struct PrimaryVolumeDescriptor {
    type_: u8,
    magic: [u8; 5],
    version: u8,
    unused: u8,
    system_identifier: [u8; 32],
    volume_identifier: [u8; 32],
    _un: [u8; 32],
    volume_space_size: [u8; 8],  // actually int32-LSB-MSB
    _un2: [u8; 32],
    volume_set_size: [u8; 4],

    // offset: 446, length: 128
    #[packed_field(bytes="446..")]
    data_preparer_identifier: [u8; 128],
    application_identifier: [u8; 128],
    copyright_file_id: [u8; 37],
    abstract_file_id: [u8; 37],
    bib_file_id: [u8; 37],
    vol_created_dt: [u8; 17],
    vol_mod_dt: [u8; 17],
    vol_expir_dt: [u8; 17],
    vol_effective_dt: [u8; 17],
}

// TODO split off into _PrimaryVolumeDescriptor and PrimaryVolumeDescriptor
// (low-level, [u8]-foo) / (high-level, str etc)
// …instead of this nonsense.
impl PrimaryVolumeDescriptor {
    pub fn system_identifier(&self) -> String {
        _bad_to_str(self.system_identifier.as_slice())
    }

    pub fn volume_identifier(&self) -> String {
        _bad_to_str(self.volume_identifier.as_slice())
    }

    pub fn data_preparer_identifier(&self) -> String {
        _bad_to_str(self.data_preparer_identifier.as_slice())
    }

    pub fn application_identifier(&self) -> String {
        _bad_to_str(self.application_identifier.as_slice())
    }
}

fn _bad_to_str(bytes: &[u8]) -> String {
    String::from_utf8(bytes.to_vec()).expect("error parsing string")
}